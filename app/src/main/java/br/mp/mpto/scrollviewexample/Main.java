package br.mp.mpto.scrollviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // configura o layout para ser apresentado em tela
        setContentView(R.layout.layout_by_code);

        // obtem uma referencia para o linear layout (onde será injetado)
        LinearLayout linearLayout = findViewById(R.id.dynamic_line);

        // obtem uma referencia para um objeto inflador
        LayoutInflater inflater = LayoutInflater.from(this);

        // infla uma celula com o objeto inflater
        // adiciona como filha de linearlayout
        for (int iIndex=0; iIndex < 20; iIndex++) {

            // obtem o layout inteiro a partir da referencia (nome do arquivo)
            LinearLayout line = (LinearLayout) inflater.inflate(R.layout.layout_cell, linearLayout, false);

            // obtem a referencia do texto dentro do layout
            TextView texto = (TextView) line.findViewById(R.id.textView_cell);

            // altera o conteúdo desse texto obtido acima
            texto.setText("Célula: " + (iIndex+1));

            // apos montada, a linha é finalmente inserida
            linearLayout.addView(line);
        }
    }
}
